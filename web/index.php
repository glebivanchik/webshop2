<?php

use App\Config\Configuration;
use Framework\Http\Routing;

function __autoload($class) {
    $class = '../'.str_replace('\\', '/', $class) . '.php';
    require_once($class);
}

session_start();
$routing = new Routing();
try {
    $html = $routing->parse();
    echo $html;
} catch(\Framework\Exceptions\NotFoundException $e) {
    header('HTTP/1.0 404 Not Found');
    $path = Configuration::getNotFoundTemplateNamePath();
    include $path;
} catch(\Exception $e) {
    echo $e->getMessage();
}

?>