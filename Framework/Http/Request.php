<?php

namespace Framework\Http;

class Request
{
    private static $postParameters;

    private static $getParameters;

    private static $isPost = false;

    /**
     * @return mixed
     */
    public function getPostParameters()
    {
        return self::$postParameters;
    }

    /**
     * @param mixed $postParameters
     */
    public function setPostParameters($postParameters)
    {
        self::$postParameters = $postParameters;
    }

    /**
     * @return mixed
     */
    public function getGetParameters()
    {
        return self::$getParameters;
    }

    /**
     * @param mixed $getParameters
     */
    public function setGetParameters($getParameters)
    {
        self::$getParameters = $getParameters;
    }

    /**
     * @return boolean
     */
    public function isPost()
    {
        return self::$isPost;
    }

    /**
     * @param boolean $isPost
     */
    public function setIsPost($isPost)
    {
        self::$isPost = $isPost;
    }

    public static function getRequestParams()
    {
        return array('get' => self::$getParameters, 'post' => self::$postParameters);
    }
}
