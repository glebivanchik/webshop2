<?php

namespace Framework\Templating;

use Framework\Http\Routing;
/**
 * Created by PhpStorm.
 * User: NotePad
 * Date: 24.08.2015
 * Time: 16:01
 */
class ViewHelper
{

    public static function url($routeName, $routeParams = array())
    {
        return Routing::generateUrl($routeName, $routeParams);
    }
}

