<?php

namespace App\Entity;

use Framework\Database\Repository;
use Framework\Http\Routing;

/**
 * Created by PhpStorm.
 * User: NotePad
 * Date: 18.08.2015
 * Time: 15:44
 */
class Category extends Repository
{
    public static $staticTableName = 'ws_category';

    private $id;

    private $name;

    private $description;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    public static function getCategoriesInfo()
    {
        $result = array();
        /** @var Category[] $categories */
        $categories = self::findBy(array());
        if (count($categories)) {
            foreach ($categories as $key => $category) {
                $goods = Good::findBy(array('category_id' => $category->getId()));
                $url = Routing::generateUrl('shop.category_item', array('id' => $category->getId()));
                $result[] = array('url' => $url, 'count' => count($goods), 'name' => $category->getName(), 'id' => $category->getId());
            }
        }

        return $result;
    }
}
