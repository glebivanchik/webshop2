<?php

namespace App\Controller;

use App\Config\Configuration;
use Framework\Controller\AbstractController;
use Framework\Http\Request;


class FeedbackController extends AbstractController
{
    private $file = 'App/Data/contact_us.txt';

    public function indexAction(Request $request)
    {
        $this->processContact($request);


        $rootDir = Configuration::getRootDir();
        $this->templateName = $rootDir.'App/View/Shop/layout_not_left_menu.tpl.php';

        $getParameters = $request->getGetParameters();
        $success = $getParameters['success'];

        return $this->render('Feedback/index.tpl.php', array('success' => $success));
    }

    private function processContact(Request $request)
    {
        $rootDir = Configuration::getRootDir();

        if ($request->isPost()) {
            $postParams = $request->getPostParameters();
            if (count($postParams)
                && $postParams['name']
                && $postParams['email']
                && $postParams['subject']
                && $postParams['message']
            ) {
                $fp = fopen ($rootDir.$this->file, "w");
                foreach ($postParams as $k => $val) {
                    fwrite($fp, $k.' = '.$val."\r\n");
                }
                fclose($fp);
            } else {
                $this->redirectTo('feedback.index', array('error' => 1));
                return false;
            }
            $this->redirectTo('feedback.index', array('success' => 1));
            return false;
        }
    }


}