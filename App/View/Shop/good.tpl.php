            <div class="col-sm-9 padding-right">
                <div class="product-details"><!--product-details-->
                    <div class="col-sm-5">
                        <div class="view-product">
                            <?php
                            if (count($good)) {
                                echo "<img src='" . $good[0]->getPicture() . "' alt='' />";
                            }
                            ?>
                            <h3>ZOOM</h3>
                        </div>
                        <div id="similar-product" class="carousel slide" data-ride="carousel">

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <a href=""><img width="80" src="<?php echo $good[0]->getPicture(); ?>" alt=""></a>
                                    <a href=""><img width="80" src="<?php echo $good[0]->getPicture(); ?>" alt=""></a>
                                    <a href=""><img width="80" src="<?php echo $good[0]->getPicture(); ?>" alt=""></a>
                                </div>
                            </div>

                            <!-- Controls -->
                            <a class="left item-control" href="#similar-product" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                            </a>
                            <a class="right item-control" href="#similar-product" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>

                    </div>
                    <div class="col-sm-7">
                        <div class="product-information"><!--/product-information-->
                            <img src="/images/product-details/new.jpg" class="newarrival" alt="" />
                            <h2><?php echo $good[0]->getName(); ?></h2>
                            <p>Web ID: <?php echo $good[0]->getId(); ?></p>
                            <img src="/images/product-details/rating.png" alt="" />
								<span>
									<span>US <?php echo $good[0]->getPrice(); ?></span>
									<label>Quantity:</label>
                                    <?php $url2 = Framework\Templating\ViewHelper::url('shop.ajax.add_to_cart', array('id' => $good[0]->getId())); ?>
									<input id="good_count" type="text" value="1" />
									<button type='button' class='btn btn-default cart add-to-cart' href='<?php echo $url2; ?>'>
                                        <i class="fa fa-shopping-cart"></i>
                                        Add to cart
                                    </button>
								</span>
                            <p><b>Category:</b> <?php echo $category; ?></p>
                            <?php include(__DIR__.'/../social_icons.tpl.php'); ?>
                        </div><!--/product-information-->
                    </div>
                </div><!--/product-details-->

                <div class="recommended_items"><!--recommended_items-->
                    <h2 class="title text-center">recommended items</h2>

                    <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="item active">
                                <?php
                                if (count($randomGoods)) {
                                    foreach ($randomGoods as $key => $value) { ?>
                                <div class="col-sm-4">
                                    <div class="product-image-wrapper">
                                        <div class="single-products">
                                            <div class="productinfo text-center">
                                                <img src="<?php echo $value->getPicture(); ?>" alt="" />
                                                <?php $url = Framework\Templating\ViewHelper::url('shop.good', array('id' => $value->getId())); ?>
                                                <h2><?php echo $value->getPrice(); ?></h2>
                                                <p></p><a href='<?php echo $url; ?>' class='link'><?php echo $value->getName(); ?></a></p><br>
                                                <button type="button" class="btn btn-default add-to-cart" href="<?= Framework\Templating\ViewHelper::url('shop.ajax.add_to_cart', array('id' => $value->getId())); ?>"><i class="fa fa-shopping-cart"></i>Add to cart</button>
                                            </div>
                                        </div>
                                    </div>
                                </div> <?php } } ?>
                            </div>
                        </div>
                        <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div><!--/recommended_items-->

            </div>


