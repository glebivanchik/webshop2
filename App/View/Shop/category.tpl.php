<div class="col-sm-9 padding-right">
    <div class="features_items"><!--features_items-->
        <h2 class="title text-center">Features Items</h2>
        <?php
        if (count($goods)) {
            foreach ($goods as $key => $value) {
                $url = Framework\Templating\ViewHelper::url('shop.good', array('id' => $value->getId()));
                include 'good_chunk.tpl.php';
            }
        }
        if ($numPages > 1) {
            echo "<ul class='pagination'>";
            for ($i = 1; $i <= $numPages; $i++) {
                $url = Framework\Templating\ViewHelper::url('shop.category_item', array('id' => $category_id, 'page' => $i));
                if ($i == $currentPage) {
                    $class = 'active';
                } else {
                    $class = '';
                }
                 echo  "<li class='".$class."'><a href='".$url."'>".$i."</a></li>";
            }
             echo "</ul>";
        }
        ?>
    </div><!--features_items-->

</div>
