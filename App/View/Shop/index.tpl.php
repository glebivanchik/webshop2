<div class="col-sm-9 padding-right">
    <div class="features_items"><!--features_items-->
        <h2 class="title text-center">Features Items</h2>
        <?php
            if (count($goods)) {
                foreach ($goods as $key => $value) {
                    $url = Framework\Templating\ViewHelper::url('shop.good', array('id' => $value->getId()));
                    include 'good_chunk.tpl.php';
                }
            }
        ?>

    </div><!--features_items-->

</div>


