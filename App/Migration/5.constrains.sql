ALTER TABLE `ws_cart`
ADD FOREIGN KEY (`goods_id`)
REFERENCES `ws_good` (`id`);

ALTER TABLE `ws_cart`
ADD FOREIGN KEY (`user_id`)
REFERENCES `ws_user` (`id`);

ALTER TABLE `ws_good`
ADD FOREIGN KEY (`category_id`)
REFERENCES `ws_category` (`id`);

ALTER TABLE `ws_order`
ADD FOREIGN KEY (`user_id`)
REFERENCES `ws_user` (`id`);

ALTER TABLE `ws_order_details`
ADD FOREIGN KEY (`order_id`)
REFERENCES `ws_order` (`id`);

ALTER TABLE `ws_order_details`
ADD FOREIGN KEY (`goods_id`)
REFERENCES `ws_good` (`id`);




